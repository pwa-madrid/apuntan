let senyores = [];
const senyorPlantillaElem = document.querySelector(".senyor").cloneNode(true);
const senyoresElem = document.querySelector("#senyores");
const nMarcadosElem = document.querySelector("#nMarcados");
const btnMarcarElem = document.querySelector("#btnMarcar");
const btnDesmarcarElem = document.querySelector("#btnDesmarcar");

fetch('https://api.mariogl.com/senyores').then(resp => {
  if (resp.ok) {
    return resp.json();
  }
}).then(datos => {
  senyores = datos.map(senyor => ({ ...senyor, marcado: false }));
  init();
});

function init() {
  limpiaSenyores();
  renderizaSenyores();
  actualizaNMarcados();
  btnMarcarElem.addEventListener('click', () => {
    marcarTodos();
  });
  btnDesmarcarElem.addEventListener('click', () => {
    desmarcarTodos();
  });
}

function limpiaSenyores() {
  document.querySelectorAll('.senyor').forEach(senyorElem => senyorElem.remove());
}

function renderizaSenyores() {
  senyores.forEach(senyor => {
    creaSenyorElem(senyor);
  });
}

function creaSenyorElem(senyor) {
  let nuevoSenyor = senyorPlantillaElem.cloneNode(true);
  nuevoSenyor.querySelector('#senyorNombre').innerText = senyor.nombre;
  nuevoSenyor.querySelector('#senyorImagen').src = 'img/' + senyor.careto;
  nuevoSenyor.querySelector('#senyorInicial').innerText = getInicial(senyor.nombre);
  nuevoSenyor.querySelector('#senyorProfesion').innerText = senyor.profesion;
  nuevoSenyor.querySelector('#senyorEstado').innerText = senyor.estado;
  nuevoSenyor.querySelector('#senyorTwitter').innerText = senyor.twitter;
  checkMarcado(senyor, nuevoSenyor);
  nuevoSenyor.addEventListener('click', e => {
    e.preventDefault();
    e.stopPropagation();
    conmutaSenyor(senyor, nuevoSenyor);
  });
  senyoresElem.appendChild(nuevoSenyor);
}

function getInicial(nombre) {
  nombre = nombre.split(' ');
  return nombre[0].length > 2 ? nombre[0].charAt(0) : nombre[1].charAt(0)
}

function conmutaSenyor(senyor, senyorElem) {
  senyor.marcado = !senyor.marcado;
  checkMarcado(senyor, senyorElem);
  actualizaNMarcados();
}

function checkMarcado(senyor, senyorElem) {
  if (senyor.marcado) {
    senyorElem.querySelector('.avatar').classList.add('marcado');
    senyorElem.querySelector('.fa-check').classList.add('visible');
  } else {
    senyorElem.querySelector('.avatar').classList.remove('marcado');
    senyorElem.querySelector('.fa-check').classList.remove('visible');
  }
}

function actualizaNMarcados() {
  const nMarcados = senyores.filter(senyor => senyor.marcado).length;
  nMarcadosElem.innerText = nMarcados;

  if (nMarcados < senyores.length) {
    btnMarcarElem.classList.add("visible");
    btnDesmarcarElem.classList.remove("visible");
  } else {
    btnMarcarElem.classList.remove("visible");
    btnDesmarcarElem.classList.add("visible");
  }
}

function marcarTodos() {
  senyores.forEach(senyor => senyor.marcado = true);
  document.querySelectorAll('.senyor').forEach((senyorElem, i) => {
    checkMarcado(senyores[i], senyorElem);
  });
  actualizaNMarcados();
}

function desmarcarTodos() {
  senyores.forEach(senyor => senyor.marcado = false);
  document.querySelectorAll('.senyor').forEach((senyorElem, i) => {
    checkMarcado(senyores[i], senyorElem);
  });
  actualizaNMarcados();
}
